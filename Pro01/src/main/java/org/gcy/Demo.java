package org.gcy;

import org.gcy.personal.Util;

/**
 * @Author: guo chunyu
 * @DateTime: 2022/3/23 3:29 下午
 */
public class Demo {
    public static void main(String[] args) {
        String uuid = Util.getUuid();
        System.out.println(uuid);
    }
}
