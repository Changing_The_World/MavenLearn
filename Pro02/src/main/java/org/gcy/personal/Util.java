package org.gcy.personal;

import java.util.UUID;

/**
 * @Author: guo chunyu
 * @DateTime: 2022/3/23 3:27 下午
 */
public class Util {

    public static String getUuid(){
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
